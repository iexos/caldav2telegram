import radicale
import radicale.config
import radicale.server
import tempfile
import threading
import socket
import requests
import time
import caldav
import pytest
import os

@pytest.fixture(scope="module")
def caldav_server():
    # shamelessly copied from https://github.com/python-caldav/caldav/blob/master/tests/test_caldav.py
    serverdir = tempfile.TemporaryDirectory()
    serverdir.__enter__()
    config = radicale.config.load("") # type: ignore
    host = "localhost:5233"
    config.update({"storage": {"filesystem_folder": serverdir.name},
                   "server": {"hosts": host}})
    server_params = {
            "url": f"http://{host}/",
            "user": "test",
            "password": "test",
            }
    server = radicale.server
    shutdown_socket, shutdown_socket_out = socket.socketpair()
    radicale_thread = threading.Thread(
        target=server.serve,
        args=(config, shutdown_socket_out),
    )
    radicale_thread.start()
    i = 0
    while True:
        try:
            requests.get(server_params["url"])
            break
        except:
            time.sleep(0.05)
            i += 1
            assert i < 100
    yield server_params
    shutdown_socket.close()
    serverdir.__exit__(None, None, None)

@pytest.fixture(scope="module")
def calendars(caldav_server):
    cal_dir = "tests/calendars"
    config = caldav_server
    calendars = {}
    caldav_client = caldav.DAVClient(url=config['url'], username=config['user'], password=config['password'])
    principal = caldav_client.principal()
    for name in [d for d in os.listdir(cal_dir) if os.path.isdir(os.path.join(cal_dir, d))]:
        curr_cal_dir = os.path.join(cal_dir, name)
        newcal = principal.make_calendar(name=name)
        for ics in [f for f in os.listdir(curr_cal_dir) if f.endswith(".ics")]:
            path = os.path.join(curr_cal_dir, ics)
            with open(path, "r") as f:
                event = f.read()
            newcal.save_event(event)
        calendars[name] = newcal.url
    return calendars
