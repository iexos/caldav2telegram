import tempfile
import pytest
from post_caldav_events.main import main

def do_integration(caldav_server, main_config):
    server_params = caldav_server
    script_config = f"""caldav:
  url: '{server_params["url"]}'
  username: '{server_params["user"]}'
  password: '{server_params["password"]}'""" + main_config
    with tempfile.NamedTemporaryFile(mode="w") as f:
        f.write(script_config)
        f.flush()
        output = main(["--config", f.name])
    return output

##################
## test configs ##
##################

def test_telegram_1d_cutoffdesc(caldav_server, calendars):
    main_config = rf"""
  calendars:
    - url: '{calendars["testing2"]}'
  offset_days: 0
  number_days: 1
  current_day_override: "2023-01-01"
format:
  text_format: 'telegram_markdown'
  time_locale: "de_DE.UTF-8"
  timezone: "Europe/Berlin"
  show_calendar_name: true
  show_location: true
  show_description: true
  cutoff_description: 20
  header_txt: ""
  footer_txt: ""
    """
    expected_output = r"""📅 *__Sonntag__ 2023\-01\-01*

__08:30__ *Long timed event* \(testing2\)
🌍 _somewhere_
here is the des\[\.\.\.\]

__09:00__ *Single Event* \(testing2\)"""
    assert do_integration(caldav_server, main_config) == expected_output


def test_text_2d_noevents(caldav_server, calendars):
    main_config = rf"""
  calendars:
    - url: '{calendars["testing"]}'
    - url: '{calendars["testing2"]}'
  offset_days: 0
  number_days: 2
  current_day_override: "2024-01-03"
format:
  text_format: 'text'
  time_locale: "de_DE.UTF-8"
  timezone: "Europe/Berlin"
  show_calendar_name: true
  show_location: true
  show_description: true
  cutoff_description: 0
  header_txt: ""
  footer_txt: ""
    """
    expected_output = r"""📅 Mittwoch - 2024-01-03

==================

📅 Donnerstag - 2024-01-04"""
    assert do_integration(caldav_server, main_config) == expected_output


def test_telegram_2d_noevents(caldav_server, calendars):
    main_config = rf"""
  calendars:
    - url: '{calendars["testing"]}'
    - url: '{calendars["testing2"]}'
  offset_days: 0
  number_days: 2
  current_day_override: "2024-01-03"
format:
  text_format: 'telegram_markdown'
  time_locale: "de_DE.UTF-8"
  timezone: "Europe/Berlin"
  show_calendar_name: true
  show_location: true
  show_description: true
  cutoff_description: 0
  header_txt: ""
  footer_txt: ""
    """
    expected_output = r"""📅 *__Mittwoch__ 2024\-01\-03*


📅 *__Donnerstag__ 2024\-01\-04*"""
    assert do_integration(caldav_server, main_config) == expected_output


def test_telegram_2d_one_calendar(caldav_server, calendars):
    main_config = rf"""
  calendars:
    - url: '{calendars["testing"]}'
  offset_days: 1
  number_days: 2
  current_day_override: "2022-12-31"
format:
  text_format: 'telegram_markdown'
  time_locale: "de_DE.UTF-8"
  timezone: "Europe/Berlin"
  show_calendar_name: false
  show_location: true
  show_description: false
  cutoff_description: 0
  header_txt: ""
  footer_txt: "here is the footer"
    """
    expected_output = r"""📅 *__Sonntag__ 2023\-01\-01*

*Multi\-All\-Day*
🌍 _with location_

*Single All\-Day*


📅 *__Montag__ 2023\-01\-02*

*Multi\-All\-Day*
🌍 _with location_

__10:00__ *\-\>test \[s@\_\+\}*
🌍 _\]\|🔋/\~ \*&^'_


here is the footer"""
    assert do_integration(caldav_server, main_config) == expected_output


def test_telegram_3d_no_features(caldav_server, calendars):
    main_config = rf"""
  calendars:
    - url: '{calendars["testing"]}'
    - url: '{calendars["testing2"]}'
  offset_days: 0
  number_days: 3
  current_day_override: "2023-01-01"
format:
  text_format: 'telegram_markdown'
  time_locale: "de_DE.UTF-8"
  timezone: "Europe/Berlin"
  show_calendar_name: false
  show_location: false
  show_description: false
  cutoff_description: 0
  header_txt: ""
  footer_txt: ""
    """
    expected_output = r"""📅 *__Sonntag__ 2023\-01\-01*

*Multi\-All\-Day*

*Single All\-Day*

__08:30__ *Long timed event*

__09:00__ *Single Event*


📅 *__Montag__ 2023\-01\-02*

*Long timed event*

*Multi\-All\-Day*

__10:00__ *\-\>test \[s@\_\+\}*


📅 *__Dienstag__ 2023\-01\-03*

*?*

*Multi\-All\-Day*

__00:00__ *?*

__00:00__ *Long timed event*

__00:00__ *Recurring twice weekly*

__07:30__ *An event without time*"""
    assert do_integration(caldav_server, main_config) == expected_output


def test_markdown_3d_no_features(caldav_server, calendars):
    main_config = rf"""
  calendars:
    - url: '{calendars["testing"]}'
    - url: '{calendars["testing2"]}'
  offset_days: 0
  number_days: 3
  current_day_override: "2023-01-01"
format:
  text_format: 'markdown'
  time_locale: "de_DE.UTF-8"
  timezone: "Europe/Berlin"
  show_calendar_name: false
  show_location: false
  show_description: false
  cutoff_description: 0
  header_txt: ""
  footer_txt: ""
    """
    expected_output = r"""#### Sonntag 2023\-01\-01
**Multi\-All\-Day**
&nbsp;
**Single All\-Day**
&nbsp;
**Long timed event**
🕒 08:30
&nbsp;
**Single Event**
🕒 09:00 - 09:25

---

#### Montag 2023\-01\-02
**Long timed event**
&nbsp;
**Multi\-All\-Day**
&nbsp;
**\-\>test \[s@\_\+}**
🕒 10:00 - 11:00

---

#### Dienstag 2023\-01\-03
**?**
&nbsp;
**Multi\-All\-Day**
&nbsp;
**?**
🕒 00:00 - 00:25
&nbsp;
**Long timed event**
🕒 00:00 - 09:25
&nbsp;
**Recurring twice weekly**
🕒 00:00 - 00:25
&nbsp;
**An event without time**
🕒 07:30 - 07:30"""
    assert do_integration(caldav_server, main_config) == expected_output


def test_text_7d_many_features(caldav_server, calendars):
    main_config = rf"""
  calendars:
    - url: '{calendars["testing"]}'
      name: 'Test calendar 1'
    - url: '{calendars["testing2"]}'
  offset_days: 2
  number_days: 7
  current_day_override: "2022-12-29"
format:
  text_format: 'text'
  time_locale: "de_DE.UTF-8"
  timezone: "Europe/Berlin"
  show_calendar_name: true
  show_location: true
  show_description: true
  cutoff_description: 0
  header_txt: "Was passiert die nächsten Tage?"
  footer_txt: "Diese Nachricht wurde automatisch generiert. Im Kalender können Termine hinzugefügt und geändert werden."
    """
    expected_output = r"""Was passiert die nächsten Tage?

==================

📅 Samstag - 2022-12-31

Multi-All-Day (Test calendar 1)
🌍 with location
🗒️ and description

==================

📅 Sonntag - 2023-01-01

Multi-All-Day (Test calendar 1)
🌍 with location
🗒️ and description

Single All-Day (Test calendar 1)

Long timed event (testing2)
🕒 08:30
🌍 somewhere
🗒️ here is the description
of this
beautiful
event

Single Event (testing2)
🕒 09:00 - 09:25

==================

📅 Montag - 2023-01-02

Long timed event (testing2)
🌍 somewhere
🗒️ here is the description
of this
beautiful
event

Multi-All-Day (Test calendar 1)
🌍 with location
🗒️ and description

->test [s@_+} (Test calendar 1)
🕒 10:00 - 11:00
🌍 ]|🔋/~ *&^'
🗒️ "#no`for.!")=(

==================

📅 Dienstag - 2023-01-03

? (Test calendar 1)

Multi-All-Day (Test calendar 1)
🌍 with location
🗒️ and description

? (testing2)
🕒 00:00 - 00:25

Long timed event (testing2)
🕒 00:00 - 09:25
🌍 somewhere
🗒️ here is the description
of this
beautiful
event

Recurring twice weekly (testing2)
🕒 00:00 - 00:25
🌍 anywhere...
🗒️ a weekly event should not have too much description, people will get tired of reading it every time

An event without time (testing2)
🕒 07:30 - 07:30

==================

📅 Mittwoch - 2023-01-04

Multi-All-Day (Test calendar 1)
🌍 with location
🗒️ and description

==================

📅 Donnerstag - 2023-01-05

Midnight to following day (testing2)

==================

📅 Freitag - 2023-01-06

Midnight to following day (testing2)
🕒 00:00 - 05:00

Recurring twice weekly (testing2)
🕒 00:00 - 00:25
🌍 anywhere...
🗒️ a weekly event should not have too much description, people will get tired of reading it every time

==================

Diese Nachricht wurde automatisch generiert. Im Kalender können Termine hinzugefügt und geändert werden."""
    assert do_integration(caldav_server, main_config) == expected_output



def test_telegram_7d_many_features(caldav_server, calendars):
    main_config = rf"""
  calendars:
    - url: '{calendars["testing"]}'
      name: 'Test calendar 1'
    - url: '{calendars["testing2"]}'
  offset_days: 2
  number_days: 7
  current_day_override: "2022-12-29"
format:
  text_format: 'telegram_markdown'
  time_locale: "de_DE.UTF-8"
  timezone: "Europe/Berlin"
  show_calendar_name: true
  show_location: true
  show_description: true
  cutoff_description: 0
  header_txt: "*Was passiert die nächsten Tage?*"
  footer_txt: >
    _Diese Nachricht wurde automatisch generiert\.
    Im [Kalender](http://caldav.example)
    können Termine hinzugefügt und geändert werden\._
    """
    expected_output = r"""*Was passiert die nächsten Tage?*


📅 *__Samstag__ 2022\-12\-31*

*Multi\-All\-Day* \(Test calendar 1\)
🌍 _with location_
and description


📅 *__Sonntag__ 2023\-01\-01*

*Multi\-All\-Day* \(Test calendar 1\)
🌍 _with location_
and description

*Single All\-Day* \(Test calendar 1\)

__08:30__ *Long timed event* \(testing2\)
🌍 _somewhere_
here is the description
of this
beautiful
event

__09:00__ *Single Event* \(testing2\)


📅 *__Montag__ 2023\-01\-02*

*Long timed event* \(testing2\)
🌍 _somewhere_
here is the description
of this
beautiful
event

*Multi\-All\-Day* \(Test calendar 1\)
🌍 _with location_
and description

__10:00__ *\-\>test \[s@\_\+\}* \(Test calendar 1\)
🌍 _\]\|🔋/\~ \*&^'_
"\#no\`for\.\!"\)\=\(


📅 *__Dienstag__ 2023\-01\-03*

*?* \(Test calendar 1\)

*Multi\-All\-Day* \(Test calendar 1\)
🌍 _with location_
and description

__00:00__ *?* \(testing2\)

__00:00__ *Long timed event* \(testing2\)
🌍 _somewhere_
here is the description
of this
beautiful
event

__00:00__ *Recurring twice weekly* \(testing2\)
🌍 _anywhere\.\.\._
a weekly event should not have too much description, people will get tired of reading it every time

__07:30__ *An event without time* \(testing2\)


📅 *__Mittwoch__ 2023\-01\-04*

*Multi\-All\-Day* \(Test calendar 1\)
🌍 _with location_
and description


📅 *__Donnerstag__ 2023\-01\-05*

*Midnight to following day* \(testing2\)


📅 *__Freitag__ 2023\-01\-06*

__00:00__ *Midnight to following day* \(testing2\)

__00:00__ *Recurring twice weekly* \(testing2\)
🌍 _anywhere\.\.\._
a weekly event should not have too much description, people will get tired of reading it every time


_Diese Nachricht wurde automatisch generiert\. Im [Kalender](http://caldav.example) können Termine hinzugefügt und geändert werden\._"""
    assert do_integration(caldav_server, main_config) == expected_output


def test_markdown_7d_many_features(caldav_server, calendars):
    main_config = rf"""
  calendars:
    - url: '{calendars["testing"]}'
      name: 'Test calendar 1'
    - url: '{calendars["testing2"]}'
  offset_days: 2
  number_days: 7
  current_day_override: "2022-12-29"
format:
  text_format: 'markdown'
  time_locale: "de_DE.UTF-8"
  timezone: "Europe/Berlin"
  show_calendar_name: true
  show_location: true
  show_description: true
  cutoff_description: 0
  header_txt: "# Was passiert die nächsten Tage?"
  footer_txt: >
    _Diese Nachricht wurde automatisch generiert\.
    Im [Kalender](http://caldav.example)
    können Termine hinzugefügt und geändert werden\._
    """
    expected_output = r"""# Was passiert die nächsten Tage?

---

#### Samstag 2022\-12\-31
**Multi\-All\-Day** (Test calendar 1)
🌍 *with location*
🗒️ and description

---

#### Sonntag 2023\-01\-01
**Multi\-All\-Day** (Test calendar 1)
🌍 *with location*
🗒️ and description
&nbsp;
**Single All\-Day** (Test calendar 1)
&nbsp;
**Long timed event** (testing2)
🕒 08:30
🌍 *somewhere*
🗒️ here is the description
of this
beautiful
event
&nbsp;
**Single Event** (testing2)
🕒 09:00 - 09:25

---

#### Montag 2023\-01\-02
**Long timed event** (testing2)
🌍 *somewhere*
🗒️ here is the description
of this
beautiful
event
&nbsp;
**Multi\-All\-Day** (Test calendar 1)
🌍 *with location*
🗒️ and description
&nbsp;
**\-\>test \[s@\_\+}** (Test calendar 1)
🕒 10:00 - 11:00
🌍 *\]\|🔋/\~ \*&^'*
🗒️ "\#no\`for.\!"\)\=\(

---

#### Dienstag 2023\-01\-03
**?** (Test calendar 1)
&nbsp;
**Multi\-All\-Day** (Test calendar 1)
🌍 *with location*
🗒️ and description
&nbsp;
**?** (testing2)
🕒 00:00 - 00:25
&nbsp;
**Long timed event** (testing2)
🕒 00:00 - 09:25
🌍 *somewhere*
🗒️ here is the description
of this
beautiful
event
&nbsp;
**Recurring twice weekly** (testing2)
🕒 00:00 - 00:25
🌍 *anywhere...*
🗒️ a weekly event should not have too much description, people will get tired of reading it every time
&nbsp;
**An event without time** (testing2)
🕒 07:30 - 07:30

---

#### Mittwoch 2023\-01\-04
**Multi\-All\-Day** (Test calendar 1)
🌍 *with location*
🗒️ and description

---

#### Donnerstag 2023\-01\-05
**Midnight to following day** (testing2)

---

#### Freitag 2023\-01\-06
**Midnight to following day** (testing2)
🕒 00:00 - 05:00
&nbsp;
**Recurring twice weekly** (testing2)
🕒 00:00 - 00:25
🌍 *anywhere...*
🗒️ a weekly event should not have too much description, people will get tired of reading it every time

---

_Diese Nachricht wurde automatisch generiert\. Im [Kalender](http://caldav.example) können Termine hinzugefügt und geändert werden\._"""
    assert do_integration(caldav_server, main_config) == expected_output


def test_no_end_time(caldav_server, calendars):
    main_config = rf"""
  calendars:
    - url: '{calendars["no_end_time"]}'
  offset_days: 0
  number_days: 1
  current_day_override: "2023-02-07"
format:
  text_format: 'text'
  time_locale: "de_DE.UTF-8"
  timezone: "Europe/Berlin"
  show_calendar_name: true
  show_location: true
  show_description: true
  cutoff_description: 250
  header_txt: ""
  footer_txt: ""
    """
    expected_output = r"""📅 Dienstag - 2023-02-07

Testevent (no_end_time)"""
    assert do_integration(caldav_server, main_config) == expected_output

@pytest.mark.skip(reason="""not sure if fixable,
                  see https://github.com/python-caldav/caldav/issues/439,
                  https://github.com/python-caldav/caldav/issues/394""")
def test_repeat_err(caldav_server, calendars):
    main_config = rf"""
  calendars:
    - url: '{calendars["repeaterr"]}'
  offset_days: 0
  number_days: 9
  current_day_override: "2023-11-07"
format:
  text_format: 'text'
  time_locale: "de_DE.UTF-8"
  timezone: "Europe/Berlin"
  show_calendar_name: true
  show_location: true
  show_description: true
  cutoff_description: 250
  header_txt: ""
  footer_txt: ""
    """
    expected_output = r"""📅 Dienstag - 2023-11-07

Normal repeat (repeaterr)

==================

📅 Mittwoch - 2023-11-08

==================

📅 Donnerstag - 2023-11-09

==================

📅 Freitag - 2023-11-10

==================

📅 Samstag - 2023-11-11

==================

📅 Sonntag - 2023-11-12

==================

📅 Montag - 2023-11-13

==================

📅 Dienstag - 2023-11-14

Exception to repeat (repeaterr)

==================

📅 Mittwoch - 2023-11-15"""
    assert do_integration(caldav_server, main_config) == expected_output
