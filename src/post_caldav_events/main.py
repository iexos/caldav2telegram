"""
main executable
"""

import argparse
import yaml
import datetime
import pytz
from operator import itemgetter
import caldav
import icalendar
from . import formats


config = {}


def date_to_datetime(date):
    return datetime.datetime.combine(date,
             datetime.datetime.min.time()).astimezone(pytz.timezone(config['format']['timezone']))


def get_day_events(caldav_client, q_start):
    q_start_dt = date_to_datetime(q_start)
    q_end_dt = q_start_dt + datetime.timedelta(days=1)

    events = []

    for calendar in config['caldav']['calendars']:
        client = caldav_client.calendar(url=calendar['url'])
        if 'name' in calendar:
            calendar_name = calendar['name']
        else:
            calendar_name = client.get_properties([caldav.dav.DisplayName()])['{DAV:}displayname'] # type: ignore
        for event in client.date_search(q_start_dt, q_end_dt):
            gevent = icalendar.Event.from_ical(event.data)
            for component in gevent.walk():
                if component.name == "VEVENT":
                    start_dt = component.get('dtstart').dt
                    try:
                        end_dt = component.get('dtend').dt
                        if not isinstance(end_dt, datetime.datetime):
                            end_dt = date_to_datetime(end_dt)
                        else:
                            end_dt = end_dt.astimezone(pytz.timezone(config['format']['timezone']))
                    except AttributeError:
                        end_dt = None
                    if not isinstance(start_dt, datetime.datetime):
                        if date_to_datetime(start_dt) >= q_end_dt:
                            continue
                        start_dt = q_start_dt
                        all_day = True
                    else:
                        start_dt = start_dt.astimezone(pytz.timezone(config['format']['timezone']))
                        if end_dt:
                            all_day = start_dt <= q_start_dt and end_dt >= q_end_dt
                        else:
                            all_day = start_dt <= q_start_dt
                        if start_dt < q_start_dt:
                            start_dt = q_start_dt
                    if all_day and end_dt != None and end_dt <= start_dt:
                        continue
                    summary = component.get('summary')
                    if summary == None:
                        summary = ""
                    events.append({
                        'calendar': calendar_name,
                        'summary': summary,
                        'location': component.get('location'),
                        'description': component.get('description'),
                        'start': start_dt,
                        'end': end_dt,
                        'all_day': all_day,
                        })
    events.sort(key=itemgetter('start', 'summary'))
    events.sort(key=itemgetter('all_day'), reverse=True)
    return events


def create_message_text(msg):
    if config['caldav']['current_day_override']:
        today = datetime.datetime.strptime(config['caldav']['current_day_override'], '%Y-%m-%d').date()
    else:
        today = datetime.datetime.now().date()
    start_day = today + datetime.timedelta(days=config['caldav']['offset_days'])

    caldav_client = caldav.DAVClient(url=config['caldav']['url'], username=config['caldav']['username'], password=config['caldav']['password'])
    day_events = {}
    for d in range(config['caldav']['number_days']):
        day = start_day + datetime.timedelta(days=d)
        day_events[day] = get_day_events(caldav_client, day)
    msg.create(day_events)
    return msg.get_text()


def main(override_args = None):
    global config
    argparser = argparse.ArgumentParser(description='Post caldav events to telegram')
    argparser.add_argument('--config', dest='config_file', help='path to config file')
    if override_args is None:
        args = argparser.parse_args()
    else:
        args = argparser.parse_args(override_args)
    with open(args.config_file, 'r') as f:
        config = yaml.load(f, Loader=yaml.FullLoader)

    text_format = config['format']['text_format']
    if text_format == 'text':
        Formatter = formats.Text
    elif text_format == 'telegram_markdown':
        Formatter = formats.TelegramMarkdownV2
    elif text_format == 'markdown':
        Formatter = formats.Markdown
    else:
        print(f"invalid text_format '{text_format}'")
        exit(1)

    msg_text = create_message_text(Formatter(config))

    return msg_text

if __name__ == '__main__':
    output = main()
    if output:
        print(output, end='')
