import locale
import pytz
import datetime

class Text:
    """
    basic text format
    """

    def __init__(self, config):
        self.config = config
        self.msg = ""

    def create_space(self):
        self.msg += ("\n")

    def create_header(self, s):
        self.msg += s + "\n"

    def create_footer(self, s):
        self.msg += s

    def create_day_header(self, weekday, date):
        self.msg += f"📅 {weekday} - {date}\n"

    def create_event_header(self, summary, calendar, start_time=None, end_time=None):
        self.msg += f"{summary}"
        if calendar:
            self.msg += f" ({calendar})"
        self.msg += "\n"
        if start_time:
            self.msg += f"🕒 {start_time}"
            if end_time:
                self.msg += f" - {end_time}"
            self.msg += "\n"

    def create_event_location(self, s):
        self.msg += f"🌍 {s}\n"

    def create_event_description(self, s):
        self.msg += f"🗒️ {s}\n"

    def create_seperation(self):
        self.msg += "\n==================\n\n"

    def create_event(self, event):
        if event['summary'] == "":
            event['summary'] = "?"
        if event['all_day']:
            start_time = None
            end_time = None
        else:
            if (event['start'] + datetime.timedelta(days=1)) <= event['end']:
                end_time = None
            else:
                end_time = event['end'].astimezone(pytz.timezone(self.config['format']['timezone'])).strftime('%H:%M')
            start_time = event['start'].astimezone(pytz.timezone(self.config['format']['timezone'])).strftime('%H:%M')
        if self.config['format']['show_calendar_name']:
            calendar = event['calendar']
        else:
            calendar = None
        self.create_event_header(event['summary'], calendar, start_time, end_time)
        if event['location'] and self.config['format']['show_location']:
            self.create_event_location(event['location'])
        if event['description'] and self.config['format']['show_description']:
            description = event['description']
            cutoff = self.config['format']['cutoff_description']
            ellipsis = "[...]"
            if cutoff > 0 and len(description) > max(cutoff, len(ellipsis)):
                description = description[:cutoff-len(ellipsis)] + ellipsis
            self.create_event_description(description)

    def create_day(self, weekday, date, events):
        self.create_day_header(weekday, date)
        for event in events:
            self.create_space()
            self.create_event(event)

    def create(self, day_events):
        """
        currently, the events are sorted by time and into days with the same
        function which pulls them via caldav. for more flexibility regarding
        message layout more refactoring is necessary
        """
        locale.setlocale(locale.LC_TIME, self.config['format']['time_locale'])
        if self.config["format"]["header_txt"]:
            self.create_header(self.config["format"]["header_txt"])
            self.create_seperation()
        first_day = True
        for day, events in day_events.items():
            if not first_day:
                self.create_seperation()
            first_day = False
            weekday = day.strftime('%A')
            date = day.strftime('%Y-%m-%d')
            self.create_day(weekday, date, events)
        if self.config["format"]["footer_txt"]:
            self.create_seperation()
            self.create_footer(self.config["format"]["footer_txt"])
        self.finalize()

    def finalize(self):
        self.msg = self.msg.strip()

    def get_text(self):
        return self.msg


class TelegramMarkdownV2(Text):
    """
    telegram markdown v2 format
    """

    def sanatize(self, text):
        """
        escape characters to use telegram markdown_v2 parse mode
        """
        if text is None:
            return ""
        escape_chars = "_*[]()~`>#+-=|{}.!"
        translate_dict = {c: "\\" + c for c in escape_chars}
        return text.translate(str.maketrans(translate_dict))

    def create_day_header(self, weekday, date):
        self.msg += f"📅 *__{self.sanatize(weekday)}__ {self.sanatize(date)}*\n"

    def create_event_header(self, summary, calendar, start_time=None, end_time=None):
        if start_time:
            self.msg += f"__{start_time}__ "
        self.msg += f"*{self.sanatize(summary)}*"
        if calendar:
            self.msg += self.sanatize(f" ({calendar})")
        self.msg += "\n"

    def create_event_location(self, s):
        self.msg += f"🌍 _{self.sanatize(s)}_\n"

    def create_event_description(self, s):
        self.msg += f"{self.sanatize(s)}\n"

    def create_seperation(self):
        self.msg += "\n\n"


class Markdown(Text):
    """
    markdown output
    built for mattermost flavor, should work with most flavors
    """

    def sanatize(self, text):
        """
        escape markdown special characters
        """
        if text is None:
            return ""
        escape_chars = "_*[]()~`>#+-=|!="
        translate_dict = {c: "\\" + c for c in escape_chars}
        return text.translate(str.maketrans(translate_dict))

    def create_day_header(self, weekday, date):
        self.msg += f"#### {self.sanatize(weekday)} {self.sanatize(date)}\n"

    def create_event_header(self, summary, calendar, start_time=None, end_time=None):
        self.msg += f"**{self.sanatize(summary)}**"
        if calendar:
            self.msg += f" ({self.sanatize(calendar)})"
        self.msg += "\n"
        if start_time:
            self.msg += f"🕒 {start_time}"
            if end_time:
                self.msg += f" - {end_time}"
            self.msg += "\n"

    def create_event_location(self, s):
        self.msg += f"🌍 *{self.sanatize(s)}*\n"

    def create_event_description(self, s):
        self.msg += f"🗒️ {self.sanatize(s)}\n"

    def create_space(self):
        self.msg += "&nbsp;\n"

    def create_seperation(self):
        self.msg += "\n---\n\n"

    def create_day(self, weekday, date, events):
        self.create_day_header(weekday, date)
        first = True
        for event in events:
            if first:
                first = False
            else:
                self.create_space()
            self.create_event(event)
