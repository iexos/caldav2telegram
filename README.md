# print-caldav-events

This script fetches calendar events via caldav (e.g. from a Nextcloud) and creates an overview over the coming days. The intended use is to automatically post this into other channels.

**Note:** Originally designed for posting directly to telegram, this script has been expanded to calling signal-cli and then been reduced to stdout only since it is used only to fire-and-forget messages which is much easier handled by a wrapping script you can use to call any API. The remaining functionality could probably be easily replaced by using [khal](https://khal.readthedocs.io/en/latest/usage.html#list).

## Setup

This is a WIP, it should be installable via `pip` in the future.

You need to install the following python packages:

- `PyYAML` (tested with `v6`)
- `pytz`
- `caldav` (`>= 0.10.1`)
- `icalendar`

## Configure

You need to create a config file. You can specify the path using `--config CONFIG_PATH` or use the default `config.yml`. You can find more information in `config.example.yml`.

## Running

Run the script via

```
cd src
python3 -m post_caldav_events.main
```

To post regular updates, create a wrapper script to send its output and schedule via cron or similar.

## Dev testing

Run `pytest`. You will need the `pytest` and `Radicale` python packages.
